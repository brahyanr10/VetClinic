import Vue from 'vue'
import Router from 'vue-router'


Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'index',
      component: () => import('./views/index.vue')
    },
    {
      path: '/login',
      name: 'Login',
      component: () => import('./views/login.vue')
    },
    {
      path: '/signUp',
      name: 'SignUp',
      component: () => import('./views/signUp.vue')
    },
    {
      path: '/forgot',
      name: 'forgot',
      component: () => import('./views/forgotYourPassword.vue')
    },
    // {
    //   path: '/info',
    //   name: 'info',
    //   component: () => import('./views/info.vue')
    // },
    {
      path: '/forgot2',
      name: 'forgot2',
      component: () => import('./views/forgot.vue')
    },
    {
      path: '/contact',
      name: 'contact',
      component: () => import('./views/contact.vue')
    },
    {
      path: '/service',
      name: 'service',
      component: () => import('./views/service.vue')
    },
    {
      path: '/profilesUser',
      name: 'profilesUser',
      component: () => import('./views/Users/profileUser.vue')
    },
    {
      path: '/profilesUser2',
      name: 'profilesUser2',
      component: () => import('./views/Users/profileUser2.vue')
    },
    {
      path: '/profilesUser3',
      name: 'profilesUser3',
      component: () => import('./views/Users/profileUser3.vue')
    },

    {
      path: '/perfilAdmi',
      name: 'navBaradmi',
      component: () => import('./views/admin/navBaradmi.vue')
    },
    {
      path: '/perfilAdmi',
      name: 'appadmi',
      component: () => import('./views/admin/appAdmin.vue')
    },
    {
      path: '/perfilAdmi/users',
      name: 'users',
      component: () => import('./views/admin/users.vue')
    }
  ]
})
