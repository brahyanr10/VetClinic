<?php
header("Access-Control-Allow-Origin: *");
header("content-type: text/html; charset=utf-8");
$conn = new mysqli( 'localhost', 'root', '', 'vetclinic' );
if ( $conn->connect_error ) {
  die( 'Error al conectarse a la base de datos' );
}
$res = array( 'error' => false );
$action = '';
if ( isset( $_GET['action'] ) ) {
  $action = $_GET['action'];
}

if($action=='read'){
  $result = $conn->query( "SELECT usuarios.nombre,
                                  usuarios.idusuarios,
                                  usuarios.idtipo_usuario,
                                  perfilpersona.calificaciones,
                                  perfilpersona.puntos,
                                  perfilpersona.imaperfil
                           from usuarios
                           INNER JOIN perfilpersona
                           on usuarios.idusuarios=perfilpersona.idusuario
                           AND usuarios.idtipo_usuario=3 " );
  $veterinarias = array();
  // echo $noticias;
  while ( $row = $result->fetch_assoc() ) {
    array_push( $veterinarias, $row );
    // echo $noticias[0];
  }
  $res['veterinarias'] = $veterinarias;

}

$conn->close();
header( 'Content-type: application/json' );
echo json_encode($res);

 ?>
