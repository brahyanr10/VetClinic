create database vetclinic;

CREATE TABLE IF NOT EXISTS `vetclinic`.`tipo_usuario` (

 `idtipo_usuario` INT NOT NULL AUTO_INCREMENT,

 `nombre_tipo` VARCHAR(20) NOT NULL,

 PRIMARY KEY (`idtipo_usuario`))
ENGINE = InnoDB;

INSERT INTO `tipo_usuario` (`idtipo_usuario`, `nombre_tipo`)
VALUES
(1, 'admin'),
(2, 'persona'),
(3, 'veterinaria');


CREATE TABLE IF NOT EXISTS `vetclinic`.`usuarios` (

  `idusuarios` INT NOT NULL AUTO_INCREMENT,

  `nombre` VARCHAR(50) NOT NULL,

  `email` VARCHAR(40) NOT NULL,

  `password` VARCHAR(20) NOT NULL,

  `idtipo_usuario` INT NOT NULL,

  PRIMARY KEY (`idusuarios`),

  FOREIGN KEY (`idtipo_usuario`)
 REFERENCES `vetclinic`.`tipo_usuario` (`idtipo_usuario`)

  ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

INSERT INTO `usuarios` (`idusuarios`, `nombre`, `email`, `password`, `idtipo_usuario`)
 VALUES (NULL, 'admin', 'admin@gmail.com', 'admin', '1');


CREATE TABLE IF NOT EXISTS `vetclinic`.`perfilpersona` (

 `idperfilpersona` INT NOT NULL AUTO_INCREMENT,

 `idusuario` INT NOT NULL,

 `imaperfil` VARCHAR(1000) NOT NULL DEFAULT 'https://www.rasoyasociados.com/se/wp-content/uploads/2018/01/sin-imagen-2.png',

 `imamuro` VARCHAR(1000) NOT NULL DEFAULT 'https://www.rasoyasociados.com/se/wp-content/uploads/2018/01/sin-imagen-2.png',

 `puntos` DECIMAL(2,0) NULL DEFAULT '0',

 `calificaciones` INT NOT NULL DEFAULT '0',

 `descripcion` VARCHAR(500) NOT NULL,

 PRIMARY KEY (`idperfilpersona`),

 FOREIGN KEY (`idusuario`)
 REFERENCES `vetclinic`.`usuarios` (`idusuarios`)

 ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `vetclinic`.`publicaciones` (

 `idpublicaciones` INT NOT NULL,

 `idusuario` INT NOT NULL,

 `descripcion` VARCHAR(500) NOT NULL,

 `fecha` TIMESTAMP(2) NOT NULL,

  PRIMARY KEY (`idpublicaciones`),

  FOREIGN KEY (`idusuario`)
 REFERENCES `vetclinic`.`usuarios` (`idusuarios`)

  ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `vetclinic`.`noticias` (

 `idnoticias` INT NOT NULL,

 `descripcion` VARCHAR(600) NOT NULL,

 `autor` VARCHAR(45) NOT NULL)
ENGINE = InnoDB;


CREATE TRIGGER `llenarperfil`
AFTER INSERT ON `usuarios`
FOR EACH ROW
INSERT INTO perfilpersona (idusuario) VALUES (NEW.idusuarioS)
